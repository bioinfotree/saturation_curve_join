### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/saturation_curve

PLOT_DATA_LEGEND ?= A. naccarii

PRJ ?= danio_1e-03

OTHERS ?= hsapiens fugu stickleback coelacanth medaka tetraodon lamprey


FULL_NL_MODEL_PARAMETERS_MEAN_ERR ?= 
FULL_FITTED_TABLE_MEAN_ERR ?= 

1_NL_MODEL_PARAMETERS_MEAN_ERR ?= 
1_FITTED_TABLE_MEAN_ERR ?= 

2_NL_MODEL_PARAMETERS_MEAN_ERR ?= 
2_FITTED_TABLE_MEAN_ERR ?= 



full_nl_model_parameters_mean_err.txt: $(FULL_NL_MODEL_PARAMETERS_MEAN_ERR)
	ln -sf $< $@
full_fitted_table_mean_err.txt: $(FULL_FITTED_TABLE_MEAN_ERR)
	ln -sf $< $@

1_nl_model_parameters_mean_err.txt: $(1_NL_MODEL_PARAMETERS_MEAN_ERR)
	ln -sf $< $@
1_fitted_table_mean_err.txt: $(1_FITTED_TABLE_MEAN_ERR)
	ln -sf $< $@

2_nl_model_parameters_mean_err.txt: $(2_NL_MODEL_PARAMETERS_MEAN_ERR)
	ln -sf $< $@
2_fitted_table_mean_err.txt: $(2_FITTED_TABLE_MEAN_ERR)
	ln -sf $< $@




# plot mean values, fitted model on mean values and error bars
CDNA1-2_saturation_plot_mean_err.pdf: full_fitted_table_mean_err.txt 1_fitted_table_mean_err.txt 2_fitted_table_mean_err.txt full_nl_model_parameters_mean_err.txt 1_nl_model_parameters_mean_err.txt 2_nl_model_parameters_mean_err.txt
	paste \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $< \
	| cut -f 1,2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^2 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^3 \
	| cut -f 2,3,8 ) \
	| sed 's/^[ \t]*//' \
	| plot_nl_model -o $@ -s -t "saturation curve" -x "reads sampled [#]" -y "different transcripts identified [#]" -q -z 1,20,1,20,1,20 --xlim 0:335000 --ylim 0:8500 -d "$(PLOT_DATA_LEGEND) cDNA Vs Danio cDNA","model: ","$(PLOT_DATA_LEGEND) male cDNA Vs Danio cDNA","model: ","$(PLOT_DATA_LEGEND) female cDNA4 Vs Danio cDNA","model: " -c "blue","red","darkgreen","lightgreen","orangered","orange" -p $^4,$^4,$^5,$^5,$^6,$^6 --in-err-file $<,$<,$^2,$^2,$^3,$^3 1,2 1,3 4,5 4,6 7,8 7,9




# plot mean values, fitted model on mean values, no error bars, no legend
CDNA1-2_saturation_plot_mean_pub.pdf: full_fitted_table_mean_err.txt 1_fitted_table_mean_err.txt 2_fitted_table_mean_err.txt full_nl_model_parameters_mean_err.txt 1_nl_model_parameters_mean_err.txt 2_nl_model_parameters_mean_err.txt
	paste \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $< \
	| cut -f 1,2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^2 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^3 \
	| cut -f 2,3,8 ) \
	| sed 's/^[ \t]*//' \
	| plot_nl_model -o $@ -s -x "reads sampled [#]" -y "different transcripts identified [#]" -q -z 1,20,1,20,1,20 --xlim 0:335000 --ylim 0:8500 -c "blue","red","darkgreen","lightgreen","orangered","orange" -p $^4,$^4,$^5,$^5,$^6,$^6 1,2 1,3 4,5 4,6 7,8 7,9



externs.mk:
	> $@; \
	for FILE in $(OTHERS); do \
	FILE_BASENAME=`echo $$FILE"_model_params" | tr '[:lower:]' '[:upper:]'`; \
	printf "extern ../../../saturation_curve_cdna/dataset/$(PRJ)/%s/phase_3/nl_model_parameters_mean_err.txt as %s\n" $$FILE $$FILE_BASENAME >> $@; \
	done; \
	for FILE in $(OTHERS); do \
	FILE_BASENAME=`echo $$FILE"_fitted_table" | tr '[:lower:]' '[:upper:]'`; \
	printf "extern ../../../saturation_curve_cdna/dataset/$(PRJ)/%s/phase_3/fitted_table_mean_err.txt as %s\n" $$FILE $$FILE_BASENAME >> $@; \
	done;

include externs.mk


MODEL_PARAMS_LN = $(addsuffix _model_params,$(OTHERS))
%_model_params: externs.mk
	ln -sf $($(call uc,$(addsuffix _model_params,$*))) $@

TABLES_LN = $(addsuffix _fitted_table,$(OTHERS))
%_fitted_table: externs.mk
	ln -sf $($(call uc,$(addsuffix _fitted_table,$*))) $@



# create string for legend
LEGEND = $(shell LEGEND=""; for i in $(OTHERS); do LEGEND="$$LEGEND,\"$(PLOT_DATA_LEGEND) cDNA Vs $$i cDNA\",\"model: \""; done; echo $$LEGEND | cut -c2-)



# plot mean values, fitted model on mean values and error bars
others_saturation_plot_mean_err.pdf: $(TABLES_LN) $(MODEL_PARAMS_LN)
	paste \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $< \
	| cut -f 1,2,3,8) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^2 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^3 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^4 \
	| cut -f 2,3,8) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^5 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^6 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^7 \
	| cut -f 2,3,8 ) \
	| sed 's/^[ \t]*//' \
	| plot_nl_model -o $@ -s -t "saturation curve" -x "reads sampled [#]" -y "different transcripts identified [#]" -q -z 1,20,1,20,1,20,1,20,1,20,1,20,1,20 --xlim 0:330000 --ylim 0:13500 -c "blue","red","darkgreen","lightgreen","orangered","orange","blueviolet","brown","burlywood","burlywood4","cadetblue3","cadetblue4","darkolivegreen3","darkolivegreen4" -d $(LEGEND) -p $^8,$^8,$^9,$^9,$^10,$^10,$^11,$^11,$^12,$^12,$^13,$^13,$^14,$^14 --in-err-file $<,$<,$^2,$^2,$^3,$^3,$^4,$^4,$^5,$^5,$^6,$^6,$^7,$^7 1,2 1,3 4,5 4,6 7,8 7,9 10,11 10,12 13,14 13,15 16,17 16,18 19,20 19,21




# plot mean values, fitted model on mean values and error bars
others_saturation_plot_mean_pub.pdf: $(TABLES_LN) $(MODEL_PARAMS_LN)
	paste \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $< \
	| cut -f 1,2,3,8) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^2 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^3 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^4 \
	| cut -f 2,3,8) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^5 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^6 \
	| cut -f 2,3,8 ) \
	<(awk '!/^[$$]/{if (NR==1) {print "\t"$$0;} else {print $$0;} }' $^7 \
	| cut -f 2,3,8 ) \
	| sed 's/^[ \t]*//' \
	| plot_nl_model -o $@ -s -x "reads sampled [#]" -y "different transcripts identified [#]" -q -z 1,20,1,20,1,20,1,20,1,20,1,20,1,20 --xlim 0:330000 --ylim 0:13500 -c "blue","red","darkgreen","lightgreen","orangered","orange","blueviolet","brown","burlywood","burlywood4","cadetblue3","cadetblue4","darkolivegreen3","darkolivegreen4" -p $^8,$^8,$^9,$^9,$^10,$^10,$^11,$^11,$^12,$^12,$^13,$^13,$^14,$^14 1,2 1,3 4,5 4,6 7,8 7,9 10,11 10,12 13,14 13,15 16,17 16,18 19,20 19,21










.PHONY: test
test:	
	LEGEND=""; \
	for i in $(OTHERS); do \
	LEGEND="$$LEGEND,\"$(PLOT_DATA_LEGEND) cDNA Vs $$i cDNA\",\"model: \""; \
	done; \
	echo $$LEGEND | cut -c2- > test.txt





# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += full_nl_model_parameters_mean_err.txt \
	 full_fitted_table_mean_err.txt \
	 1_nl_model_parameters_mean_err.txt \
	 1_fitted_table_mean_err.txt \
	 2_nl_model_parameters_mean_err.txt \
	 2_fitted_table_mean_err.txt \
	 CDNA1-2_saturation_plot_mean_err.pdf \
	 externs.mk \
	 $(MODEL_PARAMS_LN) \
	 $(TABLES_LN) \
	 others_saturation_plot_mean_err.pdf \
	 CDNA1-2_saturation_plot_mean_pub.pdf \
	 others_saturation_plot_mean_pub.pdf

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += full_nl_model_parameters_mean_err.txt \
	 full_fitted_table_mean_err.txt \
	 1_nl_model_parameters_mean_err.txt \
	 1_fitted_table_mean_err.txt \
	 2_nl_model_parameters_mean_err.txt \
	 2_fitted_table_mean_err.txt \
	 CDNA1-2_saturation_plot_mean_err.pdf \
	 externs.mk \
	 $(MODEL_PARAMS_LN) \
	 $(TABLES_LN) \
	 others_saturation_plot_mean_err.pdf \
	 CDNA1-2_saturation_plot_mean_pub.pdf \
	 others_saturation_plot_mean_pub.pdf




######################################################################
### phase_1.mk ends here
